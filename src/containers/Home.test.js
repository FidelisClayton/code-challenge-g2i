import React from 'react';

import {
  shallow,
  mount
} from 'enzyme';

import { Home } from './Home';

describe('Component: <Home />', () => {
  it('should properly render', () => {
    const wrapper = shallow (
      <Home
        questions={{
          results: [],
          loading: false
        }}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should render loading button', () => {
    const wrapper = mount(
      <Home
        questions={{
          results: [],
          loading: true
        }}
      />
    );

    expect(wrapper.find('Button').text()).toEqual('Loading...');
  });

  it('should render begin button', () => {
    const wrapper = mount(
      <Home
        questions={{
          results: [],
          loading: false
        }}
      />
    );

    expect(wrapper.find('Button').text()).toEqual('Begin');
  });

  it('should fetch quiz when click on `begin` button', () => {
    const fetchQuestions = jest.fn(() => new Promise(resolve => resolve()));
    const history = { push: jest.fn() }

    const wrapper = mount(
      <Home
        questions={{
          results: [],
          loading: false
        }}
        fetchQuestions={fetchQuestions}
        history={history}
      />
    );

    wrapper.find('Button').simulate('click');

    expect(fetchQuestions.mock.calls.length).toBe(1);
  });
});
