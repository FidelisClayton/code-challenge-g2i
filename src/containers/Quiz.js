import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { css } from 'react-emotion';

import * as colors from '../styleHelpers/colors';
import mediaQueries from '../styleHelpers/mediaQueries';

import withSwipe from '../components/withSwipe';
import QuestionCard from '../components/QuestionCard';
import Checked from '../components/icons/Checked';
import Cancel from '../components/icons/Cancel';

import {
  addAnswer,
  setCurrentQuestion
} from '../store/actions/quiz';

const styles = css(mediaQueries({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  minHeight: '100vh',
  position: 'relative',
  overflowY: 'hidden',

  '.quiz': {
    '&__category': {
      marginBottom: '40px',
      fontSize: '1.8rem',
      textAlign: 'center',
    },
    '&__tip': {
      marginTop: '35px',
      textAlign: 'center',
      maxWidth: '450px',
      display: ['block', 'none'],
      paddingLeft: '10px',
      paddingRight: '10px',
    },
    '&__actions': {
      width: '60%',
      display: 'flex',
      justifyContent: ['space-around', 'center'],
      margin: '0 auto',
      marginTop: [0, '50px'],
    },
    '&__button': {
      width: '70px',
      height: '70px',
      marginLeft: [0, '25px'],
      marginRight: [0, '25px'],
      border: '0',
      borderRadius: '50%',
      cursor: 'pointer',
      outline: 'none',

      '&:active': {
        transform: 'scale(1.1)'
      },

      '&:focus': {
        border: `2px solid ${colors.periwinkleGray}`
      }
    },
    '&__button--false': {
      backgroundColor: colors.red,
    },
    '&__button--true': {
      backgroundColor: colors.green,
    },
    '&__button-icon': {
      width: '30px',
      height: '30px',
    },
    '&__cards': {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
    }
  }
}));

export class Quiz extends Component {
  componentDidMount () {
    if (this.props.questions.loaded === false) {
      this.props.history.push('/');
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.quiz.currentQuestionIndex === this.props.questions.results.length) {
      this.props.history.push('/result');
    }

    if (!nextProps.swipe.swiping && nextProps.swipe.swiped) {
      this.handleSwipe(nextProps.swipe.direction);
      this.props.resetSwipe();
    }
  };

  nextQuestion = () => {
    this.props.setCurrentQuestion(this.props.quiz.currentQuestionIndex + 1);
  };

  handleSwipe = (direction) => {
    if (direction === 'right') {
      this.props.addAnswer('True');
    } else {
      this.props.addAnswer('False');
    }

    setTimeout(this.nextQuestion, 900);
  };

  handleTrue = () => {
    this.props.addAnswer('True');

    setTimeout(this.nextQuestion, 900);
  };

  handleFalse = () => {
    this.props.addAnswer('False');

    setTimeout(this.nextQuestion, 900);
  };

  renderCard = (question, index, array) => {
    const answersLength = this.props.quiz.answers.length;
    const lastAnswer = this.props.quiz.answers[answersLength - 1];
    const current = this.props.quiz.currentQuestionIndex;

    const canAnimate = answersLength > current;
    const firstCard = index === current;
    const secondCard = index === current + 1;
    const thirdCard = index === current + 2;

    const animation =
      lastAnswer === 'True'
        ? 'card__swipe-right'
        : 'card__swipe-left';

    const classNames = [
      (canAnimate && firstCard && this.props.swipe.swiped) && animation,
      (canAnimate && firstCard && !this.props.swipe.swiped && !this.props.swipe.xStart) && animation,
      (canAnimate && firstCard && this.props.swipe.xStart && !this.props.swipe.swiped) && 'card__hide',
      (canAnimate && secondCard) && 'card__expand',
      (canAnimate && thirdCard) && 'card__new',
    ]
      .filter(className => !!className)
      .join(' ');

    const swipeDirection = this.props.swipe.direction;
    const swipePercentage = this.props.swipe.xDiff / 150 || 0;

    const rotate = (40 * swipePercentage * -1);
    const translateX = (200 * swipePercentage * -1);

    const opacity =
      swipeDirection === 'right'
        ? 1 - (swipePercentage * 1.2) * -1
        : 1 - (swipePercentage * 1.2);

    const styles = firstCard
      ? {
        transform: firstCard ? `rotate(${Math.round(rotate)}deg) translateX(${Math.round(translateX)}px)`: 'rotate(0) translateX(0)',
        opacity: firstCard ? (Math.round(opacity * 100) / 100) : 1
      }
      : {};

    if (firstCard || secondCard || thirdCard) {
      return (
        <QuestionCard
          className={classNames}
          total={array.length}
          cardNumber={index + 1}
          question={question.question}
          answer={question.correct_answer}
          current={current}
          key={index}
          style={styles}
        />
      );
    }
  };

  render () {
    const {
      questions,
      quiz
    } = this.props;

    const currentQuestion = questions.results[quiz.currentQuestionIndex] || {};

    return (
      <section className={`${styles} quiz`}>
        <h1 className="quiz__category">{ currentQuestion.category }</h1>

        <div className="quiz__cards">
          { questions.results.map(this.renderCard) }
        </div>

        <p className="quiz__tip">Swipe <strong>left to answer false</strong> or <strong>right to answer true</strong> or click on the buttons bellow to answer</p>
        <div className="quiz__actions">
          <button
            onClick={this.handleFalse}
            className="quiz__button quiz__button--false"
          >
            <Cancel className="quiz__button-icon" />
          </button>

          <button
            onClick={this.handleTrue}
            className="quiz__button quiz__button--true"
          >
            <Checked className="quiz__button-icon" />
          </button>
        </div>
      </section>
    );
  }
}

Quiz.propTypes = {
  questions: PropTypes.shape({
    results: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    loaded: PropTypes.bool.isRequired
  }).isRequired,
  quiz: PropTypes.shape({
    answers: PropTypes.array.isRequired,
    currentQuestionIndex: PropTypes.number.isRequired
  }).isRequired,
  swipe: PropTypes.shape({
    direction: PropTypes.string,
    swiped: PropTypes.bool,
    swiping: PropTypes.bool,
    xStart: PropTypes.number,
    xDiff: PropTypes.number
  }),
  addAnswer: PropTypes.func.isRequired,
  setCurrentQuestion: PropTypes.func.isRequired,
  resetSwipe: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  questions: state.questions,
  quiz: state.quiz
});

export default connect(
  mapStateToProps,
  {
    addAnswer,
    setCurrentQuestion
  }
)(withSwipe(Quiz));
