import React from 'react';
import { connect } from 'react-redux';
import { css } from 'react-emotion';

import mediaQueries from '../styleHelpers/mediaQueries';
import { fetchQuestions } from '../store/actions/questions';

import Button from '../components/Button';

const homeStyle = css(mediaQueries({
  maxWidth: '600px',
  margin: '0 auto',
  minHeight: '100vh',
  textAlign: 'center',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: ['space-between', 'center'],
}))

export class Home extends React.Component {
  startQuiz = () => {
    this.props.fetchQuestions()
      .then(() => {
        this.props.history.push('/quiz')
      })
  }

  render () {
    return (
      <section className={homeStyle}>
        <h1>Welcome to the Trivia Challenge!</h1>

        <h2>You will be presented with 10 True or False questions.</h2>

        <h3>Can you score 100%?</h3>

        <Button onClick={this.startQuiz}>
          { this.props.questions.loading && 'Loading...' }
          { !this.props.questions.loading && 'Begin' }
        </Button>
      </section>
    )
  }
}

const mapStateToProps = (state) => ({
  questions: state.questions
})

export default connect(
  mapStateToProps,
  { fetchQuestions }
)(Home)
