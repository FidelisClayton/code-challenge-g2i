import React from 'react';

import {
  shallow,
  mount
} from 'enzyme';

import { Quiz } from './Quiz';

const questions = {
  results: [
    {
      correct_answer: 'True',
      question: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      category: 'Games'
    },
    {
      correct_answer: 'False',
      question: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      category: 'Cinema'
    },
    {
      correct_answer: 'True',
      question: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      category: 'Science'
    },
    {
      correct_answer: 'False',
      question: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      category: 'Music'
    },
  ],
  loaded: true,
  loading: false
};

const quiz = {
  answers: ['True', 'True', 'True', 'True'],
  currentQuestionIndex: 0
};

const swipe = {
  direction: null,
  swiped: false,
  xStart: null,
  xDiff: null,
  swiping: false
};

const props = {
  questions,
  quiz,
  swipe,
  addAnswer: jest.fn(),
  setCurrentQuestion: jest.fn(),
  resetSwipe: jest.fn()
};

describe('component: <Quiz />', () => {
  it('should properly render', () => {
    const wrapper = shallow(
      <Quiz {...props} />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should render the 3 first cards', () => {
    const wrapper = shallow(
      <Quiz {...props} />
    );

    expect(wrapper.find('QuestionCard').length).toEqual(3);
  });

  it('should render the category', () => {
    const wrapper = shallow(
      <Quiz {...props} />
    );

    expect(wrapper.find('.quiz__category').text()).toEqual('Games');
  });

  it('should redirect to result if quiz is complete', () => {
    const push = jest.fn();

    const wrapper = mount(
      <Quiz
        history={{ push }}
        {...props}
      />
    );

    wrapper.setProps({
      quiz: {
        ...quiz,
        currentQuestionIndex: 4
      }
    });

    expect(push.mock.calls.length).toEqual(1);
  });

  it('should handle True click', () => {
    const addAnswer = jest.fn();

    const wrapper = mount(
      <Quiz
        {...props}
        addAnswer={addAnswer}
      />
    );

    wrapper.find('.quiz__button--true').simulate('click');

    expect(addAnswer.mock.calls.length).toEqual(1);
    expect(addAnswer.mock.calls[0][0]).toEqual('True');
  });

  it('should handle False click', () => {
    const addAnswer = jest.fn();

    const wrapper = mount(
      <Quiz
        {...props}
        addAnswer={addAnswer}
      />
    );

    wrapper.find('.quiz__button--false').simulate('click');

    expect(addAnswer.mock.calls.length).toEqual(1);
    expect(addAnswer.mock.calls[0][0]).toEqual('False');
  });

  it('should add `.card__swipe-right` to the first card after choose True', () => {
    const wrapper = mount(
      <Quiz
        {...props}
        quiz={{
          answers: [ 'True' ],
          currentQuestionIndex: 0
        }}
      />
    );

    expect(wrapper.find('QuestionCard').at(0).props().className).toEqual('card__swipe-right');
  });

  it('should add `.card__swipe-left` to the first card after choose False', () => {
    const wrapper = mount(
      <Quiz
        {...props}
        quiz={{
          answers: [ 'False' ],
          currentQuestionIndex: 0
        }}
      />
    );

    expect(wrapper.find('QuestionCard').at(0).props().className).toEqual('card__swipe-left');
  });

  it('should add `.card__expand` to the second card after choose an answer', () => {
    const wrapper = mount(
      <Quiz
        {...props}
        quiz={{
          answers: [ 'False' ],
          currentQuestionIndex: 0
        }}
      />
    );

    expect(wrapper.find('QuestionCard').at(1).props().className).toEqual('card__expand');
  });

  it('should add `.card__new` to the second card after choose an answer', () => {
    const wrapper = mount(
      <Quiz
        {...props}
        quiz={{
          answers: [ 'False' ],
          currentQuestionIndex: 0
        }}
      />
    );

    expect(wrapper.find('QuestionCard').at(2).props().className).toEqual('card__new');
  });

  describe('after swipe', () => {
    it('should handle swipe', () => {
      const addAnswer = jest.fn();
      const resetSwipe = jest.fn();

      const wrapper = shallow(
        <Quiz
          {...props}
          addAnswer={addAnswer}
          resetSwipe={resetSwipe}
        />
      );

      wrapper.setProps({
        swipe: {
          ...swipe,
          swiping: false,
          swiped: true
        }
      });

      expect(addAnswer.mock.calls.length).toEqual(1);
      expect(resetSwipe.mock.calls.length).toEqual(1);
    });

    it('should add properly styles to first card after swipe right', () => {
      const wrapper = shallow(
        <Quiz
          {...props}
          quiz={{
            ...quiz,
            currentQuestionIndex: 2
          }}
        />
      );

      expect(wrapper.find('QuestionCard').at(0).props().style.transform).toEqual('rotate(0deg) translateX(0px)');
      expect(wrapper.find('QuestionCard').at(0).props().style.opacity).toEqual(1);

      wrapper.setProps({
        swipe: {
          ...swipe,
          direction: 'right',
          xDiff: 100,
        }
      });

      expect(wrapper.find('QuestionCard').at(0).props().style.transform).toEqual('rotate(-27deg) translateX(-133px)');
      expect(wrapper.find('QuestionCard').at(0).props().style.opacity).toEqual(1.8);
    });

    it('should add properly styles to first card after swipe left', () => {
      const wrapper = shallow(
        <Quiz
          {...props}
          quiz={{
            ...quiz,
            currentQuestionIndex: 2
          }}
        />
      );

      expect(wrapper.find('QuestionCard').at(0).props().style.transform).toEqual('rotate(0deg) translateX(0px)');
      expect(wrapper.find('QuestionCard').at(0).props().style.opacity).toEqual(1);

      wrapper.setProps({
        swipe: {
          ...swipe,
          direction: 'left',
          xDiff: -100,
        }
      });

      expect(wrapper.find('QuestionCard').at(0).props().style.transform).toEqual('rotate(27deg) translateX(133px)');
      expect(wrapper.find('QuestionCard').at(0).props().style.opacity).toEqual(1.8);
    });
  });
});
