import React from 'react';

import {
  shallow,
  mount
} from 'enzyme';

import { Result } from './Result';

const questions = {
  results: [
    {
      correct_answer: 'True',
      question: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
    },
    {
      correct_answer: 'False',
      question: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
    },
    {
      correct_answer: 'True',
      question: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
    },
    {
      correct_answer: 'False',
      question: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
    },
  ],
  loaded: true,
  loading: false
};

const quiz = {
  answers: ['True', 'True', 'True', 'True'],
  currentQuestionIndex: 0
};

describe('component: <Result />', () => {
  it('should properly render', () => {
    const wrapper = shallow(
      <Result
        questions={questions}
        quiz={quiz}
        reset={jest.fn()}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should render the questions', () => {
    const wrapper = shallow(
      <Result
        questions={questions}
        quiz={quiz}
        reset={jest.fn()}
      />
    );

    expect(wrapper.find('Question').length).toEqual(4);
  });

  it('should render the amount of correct answers', () => {
    const wrapper = shallow(
      <Result
        questions={questions}
        quiz={quiz}
        reset={jest.fn()}
      />
    );

    expect(wrapper.find('.home__corrects').text()).toEqual('2');
  });

  it('should render the total of questions', () => {
    const wrapper = shallow(
      <Result
        questions={questions}
        quiz={quiz}
        reset={jest.fn()}
      />
    );

    expect(wrapper.find('.home__score').text()).toEqual("2 of 4");
  });

  it('should render 2 correct answers', () => {
    const wrapper = mount(
      <Result
        questions={questions}
        quiz={quiz}
        reset={jest.fn()}
      />
    );

    expect(wrapper.find('Checked').length).toEqual(2);
  });

  it('should render 2 incorrect answers', () => {
    const wrapper = mount(
      <Result
        questions={questions}
        quiz={quiz}
        reset={jest.fn()}
      />
    );

    expect(wrapper.find('Cancel').length).toEqual(2);
  });

  it('should redirect to home if quiz is not loaded', () => {
    const push = jest.fn();

    const wrapper = shallow(
      <Result
        questions={{
          ...questions,
          loaded: false
        }}
        quiz={quiz}
        reset={jest.fn()}
        history={{ push }}
      />
    );

    expect(push.mock.calls.length).toEqual(1);
  });

  it('should reset state and redirect to home home when click in `Play Again?`', () => {
    const push = jest.fn();
    const reset = jest.fn();

    const wrapper = shallow(
      <Result
        questions={questions}
        quiz={quiz}
        reset={reset}
        history={{ push }}
      />
    );

    wrapper.find('Button').simulate('click');

    expect(push.mock.calls.length).toEqual(1);
    expect(reset.mock.calls.length).toEqual(1);
  });
});
