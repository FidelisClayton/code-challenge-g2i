import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { css } from 'react-emotion';

import * as colors from '../styleHelpers/colors';
import mediaQueries from '../styleHelpers/mediaQueries';
import { reset } from '../store/actions/quiz';

import Question from '../components/Question';
import Button from '../components/Button';

const styles = css(mediaQueries({
  display: 'flex',
  maxWidth: '600px',
  margin: '0 auto',
  flexDirection: 'column',
  alignItems: ['normal', 'center'],
  justifyContent: 'space-between',
  minHeight: '100vh',
  paddingBottom: [0, '30px'],

  '.home': {
    '&__title': {
      textAlign: 'center',
      marginBottom: 0,
      color: colors.white,
    },
    '&__score': {
      textAlign: 'center',
      marginTop: 0,
      fontWeight: 300,
    },
    '&__corrects': {
      fontSize: '3rem',
      fontWeight: '500',
      color: colors.white,
    },
    '&__play-again': {
      marginTop: [0, '30px'],
    }
  }
}));

const countCorrects = (questions, answers) => {
  return questions.filter(
    (question, index) => question.correct_answer === answers[index]
  ).length;
};

export class Result extends Component {
  componentDidMount () {
    if (!this.props.questions.loaded) {
      this.goToHome();
    }
  }

  goToHome = () => {
    this.props.history.push('/');
  }

  playAgain = () => {
    this.props.reset();
    this.goToHome();
  }

  render () {
    const {
      questions,
      quiz,
    } = this.props;

    return (
      <section className={`${styles} result`}>
        <div className="home__header">
          <h1 className="home__title">
            You scored
          </h1>
          <h2 className="home__score">
            <span className="home__corrects">{ countCorrects(questions.results, quiz.answers)}</span> of { questions.results.length }
          </h2>
        </div>

        <div className="home__questions">
          { questions.results.map((question, index) => (
            <Question
              key={index}
              question={question.question}
              correct={question.correct_answer === quiz.answers[index]}
            />
          ))}
        </div>

        <Button
          onClick={this.playAgain}
          className="home__play-again"
        >
          Play Again?
        </Button>
      </section>
    );
  }
}

Result.propTypes = {
  questions: PropTypes.shape({
    results: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    loaded: PropTypes.bool.isRequired
  }),
  quiz: PropTypes.shape({
    answers: PropTypes.array.isRequired,
    currentQuestionIndex: PropTypes.number.isRequired
  }),
  reset: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  questions: state.questions,
  quiz: state.quiz
});

export default connect(mapStateToProps, { reset })(Result);
