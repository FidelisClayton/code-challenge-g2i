import React from 'react';
import { shallow } from 'enzyme';

import QuestionCard from './QuestionCard';

describe('Component: <QuestionCard />', () => {
  it ('should render properly', () => {
    const wrapper = shallow(
      <QuestionCard
        cardNumber={1}
        total={10}
        question="Curabitur aliquet quam id dui posuere blandit. Donec rutrum congue leo eget malesuada."
        answer="True"
      />
    );

    expect(wrapper).toMatchSnapshot();
  });
});
