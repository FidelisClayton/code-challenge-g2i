import React from 'react';
import { shallow } from 'enzyme';

import Question from './Question';

describe('Component: <Question />', () => {
  it('should render properly', () => {
    const wrapper = shallow(
      <Question
        question="Proin eget tortor risus. Nulla porttitor accumsan tincidunt."
        correct={true}
      />
    )

    expect(wrapper).toMatchSnapshot();
  });

  describe('when the answer is correct', () => {
    it('should render `<Checked />`', () => {
      const wrapper = shallow(
        <Question
          question="Proin eget tortor risus. Nulla porttitor accumsan tincidunt."
          correct={true}
        />
      );

      expect(wrapper.find('Checked')).toHaveLength(1);
    });

    it('should not render `<Cancel />`', () => {
      const wrapper = shallow(
        <Question
          question="Proin eget tortor risus. Nulla porttitor accumsan tincidunt."
          correct={true}
        />
      );

      expect(wrapper.find('Cancel')).toHaveLength(0);
    });
  });

  describe('when the answer is incorrect', () => {
    it('should render `<Cancel />`', () => {
      const wrapper = shallow(
        <Question
          question="Proin eget tortor risus. Nulla porttitor accumsan tincidunt."
          correct={false}
        />
      );

      expect(wrapper.find('Cancel')).toHaveLength(1);
    });

    it('should not render `<Checked />`', () => {
      const wrapper = shallow(
        <Question
          question="Proin eget tortor risus. Nulla porttitor accumsan tincidunt."
          correct={false}
        />
      );

      expect(wrapper.find('Checked')).toHaveLength(0);
    });
  });
});
