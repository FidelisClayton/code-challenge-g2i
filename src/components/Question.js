import React from 'react';
import PropTypes from 'prop-types';
import { css } from 'react-emotion';

import Checked from '../components/icons/Checked';
import Cancel from '../components/icons/Cancel';

const styles = css({
  display: 'flex',
  padding: '15px',

  '.question': {
    '&__result': {
      flexBasis: '50px',
      flexShrink: 0,
      textAlign: 'center',
    },
    '&__icon': {
      width: '30px',
      height: '30px',
    },
    '&__text': {
      fontSize: '1.4rem',
      wordWrap: 'break-word',
    },
    '&__incorrect': {
      width: '25px',
      height: '25px',
    }
  }
})

const Question = ({
  question,
  correct,
  ...props,
}) => (
  <div
    className={`${styles} question`}
    {...props}
  >
    <div className="question__result">
      { correct
          ? <Checked className="question__icon" />
          : <Cancel className="question__icon question__incorrect" />
      }
    </div>

    <div
      className="question__text"
      dangerouslySetInnerHTML={{ __html: question }}
    />
  </div>
);

Question.propTypes = {
  question: PropTypes.string.isRequired,
  correct: PropTypes.bool.isRequired
};

export default Question;
