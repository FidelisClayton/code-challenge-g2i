import React from 'react';
import PropTypes from 'prop-types';
import { css } from 'react-emotion';

import * as colors from '../styleHelpers/colors';
import * as animations from '../styleHelpers/animations';

const cardStyle = css({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  backgroundColor: 'white',
  position: 'relative',
  minHeight: '350px',
  maxHeight: '45vh',
  maxWidth: '450px',
  margin: '0 auto',
  width: '80%',
  boxShadow: `0px 0px 15px 3px ${colors.shadow}`,
  padding: '20px',

  '&.card': {
    '&__swipe-right': {
      animation: `${animations.swipeRight} 500ms alternate forwards`
    },

    '&__swipe-left': {
      animation: `${animations.swipeLeft} 500ms alternate forwards`
    },

    '&__expand': {
      animation: `${animations.expand} 300ms alternate forwards 100ms`
    },

    '&__new': {
      animation: `${animations.expandNew} 300ms alternate forwards 150ms`,
    },

    '&__hide': {
      visibility: 'hidden',
    }
  },

  '.card': {
    '&__progress': {
      fontSize: '1.3rem',
      position: 'absolute',
      top: '15px',
    },
    '&__current': {
      fontSize: '3rem',
      borderBottom: `3px solid ${colors.indigo}`,
      color: colors.indigo,
    },
    '&__question': {
      fontSize: '1.4rem',
      color: colors.darkGrey,
      textAlign: 'center',
      wordWrap: 'break-word',
    },
  },

  '&:nth-of-type(1)': {
    zIndex: 3,
  },

  '&:nth-of-type(2)': {
    position: 'absolute',
    transform: 'scale(0.8)',
    marginTop: '60px',
    zIndex: 2,
  },

  '&:nth-of-type(3)': {
    position: 'absolute',
    transform: 'scale(0)',
    marginTop: '60px',
    zIndex: 1,
  }
});

const QuestionCard = ({
  cardNumber,
  total,
  question,
  answer,
  className,
  ...props
}) => {
  return (
    <div
      className={`${cardStyle} card ${className}`}
      {...props}
    >
      <div className="card__progress">
        <span className="card__current">{ cardNumber }</span> of { total }
      </div>

      <p
        className="card__question"
        dangerouslySetInnerHTML={{ __html: question }}
      />
    </div>
  );
};

QuestionCard.propTypes = {
  cardNumber: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
  question: PropTypes.string.isRequired,
  answer: PropTypes.string.isRequired,
  className: PropTypes.string
}

export default QuestionCard
