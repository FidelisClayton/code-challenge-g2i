import React, { Component } from 'react';

const defaultConfigs = {
  distanceToSwipe: 100
};

const initialState = {
  xStart: null,
  xDiff: 0,
  xCurrent: null,
  direction: null,
  swiping: false,
  swiped: false
};

const withSwipe = (WrappedComponent, configs = defaultConfigs) => {
  class WithSwipe extends Component {
    state = initialState;

    componentDidMount() {
      document.addEventListener('touchstart', this.handleTouchStart);
      document.addEventListener('touchend', this.handleTouchEnd);
      document.addEventListener('touchmove', this.handleTouchMove);
    }

    componentWillUnmount() {
      document.removeEventListener('touchstart', this.handleTouchStart);
      document.removeEventListener('touchend', this.handleTouchEnd);
      document.removeEventListener('touchmove', this.handleTouchMove);
    }

    handleTouchStart = (event) => {
      this.setState({
        xStart: event.touches[0].clientX,
      })
    };

    handleTouchEnd = () => {
      const xDiff = Math.abs(this.state.xStart - (this.state.xCurrent || this.state.xStart));
      const swiped = xDiff > configs.distanceToSwipe;

      if (swiped) {
        this.setState({
          swiping: false,
          swiped
        });
      } else {
        this.setState({
          xStart: null,
          xCurrent: null,
          xDiff: 0,
          swiping: false,
          swiped
        });
      }
    };

    handleTouchMove = (event) => {
      const {
        xStart,
      } = this.state;

      if (!xStart) {
        return;
      }

      const xCurrent = event.touches[0].clientX;

      const xDiff = xStart - xCurrent;

      const data = {
        xCurrent,
        xDiff,
        xStart,
      };

      if (Math.abs(xDiff) > 0) {
        if (xDiff > 0) {
          this.swipe(data, 'left');
        } else {
          this.swipe(data, 'right');
        }
      }
    };

    swipe = (data, direction) => {
      this.setState({
        ...data,
        direction,
        swiping: true
      });
    };

    resetSwipe = () => {
      this.setState({
        ...initialState,
        xStart: this.state.xStart
      });
    };

    render () {
      return (
        <WrappedComponent
          swipe={this.state}
          resetSwipe={this.resetSwipe}
          {...this.props}
        />
      );
    }
  }

  WithSwipe.displayName = `WithSwipe(${getDisplayName(WrappedComponent)})`;

  return WithSwipe;
};

const getDisplayName = (WrappedComponent) => {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
};

export default withSwipe;
