import React from 'react';
import { css } from 'react-emotion';

import * as colors from '../styleHelpers/colors';
import * as fonts from '../styleHelpers/fonts';
import mediaQueries from '../styleHelpers/mediaQueries';

const buttonStyle = css(mediaQueries({
  height: ['60px', '50px'],
  width: ['100%', '200px'],
  paddingTop: '10px',
  paddingBottom: '10px',
  border: 0,
  fontSize: '1rem',
  backgroundColor: colors.darkIndigo,
  color: colors.white,
  textTransform: 'uppercase',
  fontFamily: fonts.raleway,
  fontWeight: '700',
  outline: 'none',

  '&:focus': {
    border: `2px solid ${colors.periwinkleGray}`
  }
}));

const Button = ({
  children,
  className,
  ...props
}) => (
  <button
    className={`${buttonStyle} ${className || ''}`}
    {...props}
  >
    { children }
  </button>
);

export default Button;
