import React from 'react';
import {
  shallow,
  mount
} from 'enzyme';

import withSwipe from './withSwipe';

const ComponentWithSwipe = withSwipe(() => <h1>Hello</h1>);

describe('Component: withSwipe', () => {
  it('should pass swipe props to children', () => {
    const wrapper = shallow(
      <ComponentWithSwipe />
    );

    const swipeProps = {
      xStart: null,
      xDiff: 0,
      xCurrent: null,
      direction: null,
      swiping: false,
      swiped: false
    };

    expect(wrapper.props().swipe).toEqual(swipeProps)
    expect(wrapper.props().resetSwipe).not.toBeNull()
  });

  it('should update xStart when touch start', () => {
    const wrapper = shallow(
      <ComponentWithSwipe />
    );

    const instance = wrapper.instance();

    instance.handleTouchStart({
      touches: [
        { clientX: 50 }
      ]
    });

    expect(instance.state.xStart).toEqual(50)
  });

  describe('should update starte with swipe data', () => {
    it('when swiping left', () => {
      const wrapper = shallow(
        <ComponentWithSwipe />
      );

      const instance = wrapper.instance();

      const expectedState = {
        xStart: 50,
        xDiff: 350,
        xCurrent: -300,
        direction: 'left',
        swiping: true,
        swiped: false
      };

      instance.handleTouchStart({ touches: [ { clientX: 50 } ] });
      instance.handleTouchMove({ touches: [ { clientX: -300 } ] });

      expect(instance.state).toEqual(expectedState);
    });

    it('when swiping right', () => {
      const wrapper = shallow(
        <ComponentWithSwipe />
      );

      const instance = wrapper.instance();

      const expectedState = {
        xStart: 50,
        xDiff: -250,
        xCurrent: 300,
        direction: 'right',
        swiping: true,
        swiped: false
      };

      instance.handleTouchStart({ touches: [ { clientX: 50 } ] });
      instance.handleTouchMove({ touches: [ { clientX: 300 } ] });

      expect(instance.state).toEqual(expectedState);
    });

    it('after swipe left', () => {
      const wrapper = shallow(
        <ComponentWithSwipe />
      );

      const instance = wrapper.instance();

      const expectedState = {
        xStart: 50,
        xDiff: 350,
        xCurrent: -300,
        direction: 'left',
        swiping: false,
        swiped: true
      };

      instance.handleTouchStart({ touches: [ { clientX: 50 } ] });
      instance.handleTouchMove({ touches: [ { clientX: -300 } ] });
      instance.handleTouchEnd();

      expect(instance.state).toEqual(expectedState);
    });

    it('after swipe right', () => {
      const wrapper = shallow(
        <ComponentWithSwipe />
      );

      const instance = wrapper.instance();

      const expectedState = {
        xStart: 50,
        xDiff: -250,
        xCurrent: 300,
        direction: 'right',
        swiping: false,
        swiped: true
      };

      instance.handleTouchStart({ touches: [ { clientX: 50 } ] });
      instance.handleTouchMove({ touches: [ { clientX: 300 } ] });
      instance.handleTouchEnd();

      expect(instance.state).toEqual(expectedState);
    });
  });

  it('should reset the swipe state', () => {
    const wrapper = shallow(
      <ComponentWithSwipe />
    );

    const instance = wrapper.instance();

    const expectedState = {
      xStart: 50,
      xDiff: 0,
      xCurrent: null,
      direction: null,
      swiping: false,
      swiped: false
    };

    instance.handleTouchStart({ touches: [ { clientX: 50 } ] });
    instance.handleTouchMove({ touches: [ { clientX: 300 } ] });
    instance.handleTouchEnd();
    instance.resetSwipe();

    expect(instance.state).toEqual(expectedState);
  })
});
