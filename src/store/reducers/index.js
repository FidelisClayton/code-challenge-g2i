import questions from './questions';
import quiz from './quiz';

export default {
  questions,
  quiz
};
