import * as actions from '../actions/questions';

export const initialState = {
  results: [],
  loading: false,
  loaded: false,
  error: null
};

const questionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.FETCH_QUESTIONS_REQUEST:
      return {
        ...state,
        loading: true,
        error: null
      };

    case actions.FETCH_QUESTIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        results: action.payload.results
      };

    case actions.FETCH_QUESTIONS_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload
      };

    case actions.RESET:
      return initialState;

    default:
      return state;
  }
};

export default questionsReducer;
