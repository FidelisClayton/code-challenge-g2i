import reducer, { initialState } from './questions';

describe('reducer: Questions', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle `FETCH_QUESTIONS_REQUEST`', () => {
    expect(
      reducer(initialState, {
        type: 'FETCH_QUESTIONS_REQUEST'
      })
    ).toEqual({
      ...initialState,
      loading: true
    });
  });

  it('should handle `FETCH_QUESTIONS_SUCCESS`', () => {
    expect(
      reducer(initialState, {
        type: 'FETCH_QUESTIONS_SUCCESS',
        payload: {
          results: [{ correct_answer: 'True', question: 'Lorem Ipsum'}]
        }
      })
    ).toEqual({
      ...initialState,
      results: [{ correct_answer: 'True', question: 'Lorem Ipsum' }],
      loaded: true,
      loading: false
    });
  });

  it('should handle `FETCH_QUESTIONS_FAIL`', () => {
    expect(
      reducer(initialState, {
        type: 'FETCH_QUESTIONS_FAIL',
        payload: 'error'
      })
    ).toEqual({
      ...initialState,
      loaded: false,
      loading: false,
      error: 'error'
    });
  });

  it('should handle `RESET`', () => {
    expect(
      reducer(initialState, {
        type: 'RESET'
      })
    ).toEqual(initialState);
  });
});
