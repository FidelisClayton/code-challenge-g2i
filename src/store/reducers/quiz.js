import * as actions from '../actions/quiz';

export const initialState = {
  answers: [],
  currentQuestionIndex: 0
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.ADD_ANSWER:
      return {
        ...state,
        answers: [ ...state.answers, action.payload ]
      };

    case actions.SET_CURRENT_QUESTION:
      return {
        ...state,
        currentQuestionIndex: action.payload
      };

    case actions.RESET:
      return initialState;

    default:
      return state;
  }
};

export default reducer;
