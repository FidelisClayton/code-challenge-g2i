import reducer, { initialState } from './quiz';

describe('reducer: Quiz', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle `ADD_ANSWER`', () => {
    expect(
      reducer(initialState, {
        type: 'ADD_ANSWER',
        payload: 'True'
      })
    ).toEqual({
      ...initialState,
      answers: ['True']
    });
  });

  it('should handle `CURRENT_QUESTION`', () => {
    expect(
      reducer(initialState, {
        type: 'CURRENT_QUESTION',
        payload: 1
      })
    ).toEqual({
      ...initialState,
      currentQuestionIndex: 1
    });
  });

  it('should handle `RESET`', () => {
    expect(
      reducer(initialState, {
        type: 'RESET'
      })
    ).toEqual(initialState);
  });
});
