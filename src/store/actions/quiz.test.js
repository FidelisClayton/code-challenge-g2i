import * as actions from './quiz';

describe('action creators: Quiz', () => {
  it('should properly create `ADD_ANSWER` action', () => {
    expect(actions.addAnswer('True')).toEqual({
      type: actions.ADD_ANSWER,
      payload: 'True'
    });
  });

  it('should properly create `CURRENT_QUESTION` action', () => {
    expect(actions.setCurrentQuestion(1)).toEqual({
      type: actions.SET_CURRENT_QUESTION,
      payload: 1
    });
  });

  it('should properly create `RESET` action', () => {
    expect(actions.reset()).toEqual({
      type: actions.RESET
    });
  });
});
