import fetchMock from 'fetch-mock';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import * as actions from './questions';

const middlewares = [ thunk ];
const mockStore = configureMockStore(middlewares);

const fakeData = {
  "response_code": 0,
  "results": [
    {
      "category": "Science & Nature",
      "type": "boolean",
      "difficulty": "hard",
      "question": "You can calculate Induced Voltage using: &epsilon; =-N * (d&Phi;B)\/(d)",
      "correct_answer": "False",
      "incorrect_answers":["True"]
    }
  ]
};

let mock;

describe('action creators: Questions', () => {
  beforeEach(() => {
    mock = new MockAdapter(axios);
  });

  it('should create `FETCH_QUESTIONS_REQUEST` and `FETCH_QUESTIONS_SUCCESS` after fetch questions', () => {
    mock
      .onGet('https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean')
      .reply(200, fakeData);

    const expectedActions = [
      { type: actions.FETCH_QUESTIONS_REQUEST },
      { type: actions.FETCH_QUESTIONS_SUCCESS, payload: fakeData }
    ];

    const store = mockStore({})

    return store.dispatch(actions.fetchQuestions())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });

  it('should create `FETCH_QUESTIONS_REQUEST` and `FETCH_QUESTIONS_FAIL` after fetch questions with failure', () => {
    mock
      .onGet('https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean')
      .reply(500, { error: 'error '});

    const expectedActions = [
      { type: actions.FETCH_QUESTIONS_REQUEST },
      { type: actions.FETCH_QUESTIONS_FAIL, payload: new Error('Request failed with status code 500') }
    ];

    const store = mockStore({});

    return store.dispatch(actions.fetchQuestions())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });
});
