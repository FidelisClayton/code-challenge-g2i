export const ADD_ANSWER = 'ADD_ANSWER';
export const SET_CURRENT_QUESTION = 'CURRENT_QUESTION';
export const RESET = 'RESET';

export const addAnswer = (answer) => ({
  type: ADD_ANSWER,
  payload: answer
});

export const setCurrentQuestion = (index) => ({
  type: SET_CURRENT_QUESTION,
  payload: index
});

export const reset = () => ({
  type: RESET
})
