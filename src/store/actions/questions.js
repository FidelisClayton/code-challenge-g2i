import { getQuestions } from '../../api';

export const FETCH_QUESTIONS_REQUEST = 'FETCH_QUESTIONS_REQUEST';
export const FETCH_QUESTIONS_SUCCESS = 'FETCH_QUESTIONS_SUCCESS';
export const FETCH_QUESTIONS_FAIL = 'FETCH_QUESTIONS_FAIL';

export const RESET = 'RESET';

const fetchQuestionsRequest = () => ({
  type: FETCH_QUESTIONS_REQUEST,
})

const fetchQuestionsSuccess = (payload) => ({
  type: FETCH_QUESTIONS_SUCCESS,
  payload
})

const fetchQuestionsFail = (payload) => ({
  type: FETCH_QUESTIONS_FAIL,
  payload
})

export const fetchQuestions = () => (dispatch) => {
  dispatch(fetchQuestionsRequest())

  return getQuestions()
    .then((questions) => dispatch(fetchQuestionsSuccess(questions)))
    .catch((error) => dispatch(fetchQuestionsFail(error)))
}
