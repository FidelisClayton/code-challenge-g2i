import thunk from 'redux-thunk';

import {
  routerReducer,
  routerMiddleware
} from 'react-router-redux';

import {
  createStore,
  applyMiddleware,
  compose,
  combineReducers
} from 'redux';

import reducers from './reducers';
import { history } from '../utils/history';

const environment = process.env.NODE_ENV

const composeEnhancers =
  (environment === 'development' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__)
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : compose;

const store = createStore(
  combineReducers({
    ...reducers,
    routing: routerReducer
  }),
  composeEnhancers(
    applyMiddleware(
      thunk,
      routerMiddleware(history)
    )
  )
);

export default store;
