import React, { Component } from 'react';
import { css } from 'react-emotion';

import Router from './Router';

import * as colors from './styleHelpers/colors';
import * as fonts from './styleHelpers/fonts';

const appStyle = css({
  color: colors.periwinkleGray,
  fontFamily: fonts.raleway,
  margin: '0 auto',
  minHeight: '100vh',
})

class App extends Component {
  render() {
    return (
      <div className={appStyle}>
        <Router />
      </div>
    );
  }
}

export default App;
