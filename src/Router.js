import React from 'react';

import {
  Router,
  Switch,
  Route
} from 'react-router-dom';

import { history } from './utils/history';
import Home from './containers/Home';
import Quiz from './containers/Quiz';
import Result from './containers/Result';

const AppRouter = () => (
  <Router history={history}>
    <Switch>
      <Route
        exact
        path="/"
        component={Home}
      />

      <Route
        exact
        path="/quiz"
        component={Quiz}
      />

      <Route
        path="/result"
        component={Result}
      />

      <Route
        path="*"
        component={Home}
      />
    </Switch>
  </Router>
);

export default AppRouter;
