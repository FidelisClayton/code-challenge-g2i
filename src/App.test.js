import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import App from './App';

const initialState = {
  questions: {
    results: [],
    loading: false,
    loaded: false,
    error: null
  },
  quiz: {
    answers: [],
    currentQuestionIndex: 0
  }
};

it('renders without crashing', () => {
  const mockStore = configureStore();
  const store = mockStore(initialState);

  const div = document.createElement('div');

  ReactDOM.render(
    <Provider store={store}>
      <App store={store} />
    </Provider>,
  div);

  ReactDOM.unmountComponentAtNode(div);
});
