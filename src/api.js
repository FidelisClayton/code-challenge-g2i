import axios from 'axios';

const endpoint = 'https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean';

export const getQuestions = () => {
  return axios.get(endpoint)
  .then(response => response.data)
};
