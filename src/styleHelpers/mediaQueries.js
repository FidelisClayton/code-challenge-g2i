import facepaint from 'facepaint';

export const extraSmall = '480px';
export const small = '768px';
export const medium = '960px';
export const large = '1120px';
export const extraLarge = '1280px';

const mediaQueries = facepaint([
  `@media(min-width: ${small})`,
  `@media(min-width: ${medium})`
]);

export default mediaQueries;
