import { keyframes } from 'react-emotion';

export const swipeRight = keyframes`
  0% {
    transform: rotate(0) translateX(0) scale(1);
    opacity: 1;
  }

  100% {
    transform: rotate(40deg) translateX(200px);
    opacity: 0;
  }
`;

export const swipeLeft = keyframes`
  0% {
    transform: rotate(0) translateX(0) scale(1);
    opacity: 1;
  }

  100% {
    transform: rotate(-40deg) translateX(-200px);
    opacity: 0;
  }
`;

export const expand = keyframes`
  0% {
    transform: scale(0.8);
  }

  100% {
    transform: scale(1);
    margin-top: 0;
  }
`;

export const expandNew = keyframes`
  0% {
    transform: scale(0);
  }

  100% {
    transform: scale(0.8);
  }
`;
