export const indigo = '#3F51B5';
export const darkIndigo = '#303F9F';
export const red = '#F44336';
export const darkRed = '#D32F2F';
export const green = '#4CAF50';
export const darkGreen = '#388E3C';
export const darkGrey = '#A4A9C5';

export const periwinkleGray = '#C5CAE9';
export const white = '#FFFFFF';

export const shadow = 'rgba(0, 0, 0, 0.3)';
